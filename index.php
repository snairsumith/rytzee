<?php
get_header(); 
get_sidebar();
?>
<!-- About Section Start -->
<span id="benefit"></span>
<div id="about" class="pad-0">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 benifit-background-div">
        <div class="benifit-div">
          <h3 class="benefit-h3">BENEFITS</h3>
          <p class="benefit-p">Rytzee is a composite technology platform, that stimulates maximum revenue for Travel and hospitality companies and fosters guest satisfaction.</p>
        </div>
      </div>
    </div>
    <div  class="row">
      <div class="col-md-12 benifit-background">
        <div class="video-text">
          <p class="video-text-p1"> Free personal concierge experiences to your guests at their finger tips</p>
          <h3 class="video-text-h3">DELIGHT YOUR GUESTS</h3>
          <p class="video-text-p1">With The Rytzee Experience</p>
          <button data-toggle="modal" data-src="https://www.youtube.com/embed/-uQ_MT3N4DQ" class="btn btn-lg btn-common animated fadeInUp btn-background-color video-btn" data-target="#myModal">Watch Video</button>
        </div>
      </div>
      
    </div>
    <div class="row guest-div">
      <div class="col-md-6 guest-div1">
        <div class="guest-div-inner inner-img1">
          <h3 class="guest-inner-h3">Personalized Guest experience</h3>
          <p class="guest-inner-p">The Rytzee application is designed, to ensure that travelers have a well-structured and delightful vacation experience at their fingertips, with numerous revolutionary features including a budget meter, concierge services, and a flawless payment and reservation procedure via the Rytzee mobile interface. Rytzee facilitates the ideal travel experience with an integrated map view to visualize your day and plan your vacation efficiently. In fact, what makes Rytzee unique, is that its designed just for you, with timely notifications and reminders on nearby offers, schedules and a docket to maintain travel profiles and preferences effortlessly. Rytzee connects guests to concierge services and local specialties from anywhere, anytime.</p>
        </div>
      </div>
      <div class="col-md-6 guest-div2">
        <div class="guest-div-inner inner-img2">
          <h3 class="guest-inner-h3">Earn Incremental Revenue</h3>
          <p class="guest-inner-p">The hallmark of this application, however, lies in its ability to allow travel and hospitality firms to earn incremental revenue, by advertising their own services, and connecting Rytzee users to local partners offering specialized services thereby earning referral revenue for the hospitality agents from local partners. Our cloud-based platform allows hoteliers to suggest customized travel plans, services and products to guests and access all relevant information in a cohesive dashboard. The Rytzee platform is designed to assist hospitality firms in promoting and upselling their services, easily accept payment from customers and local partners, set custom pricing for their products. Gain insights for maximizing customer satisfaction and gross profit. It lets the hotel communicate with its guests, receive feedback on services, keep in touch with its guest through mailers, anniversary wishes. </p>
        </div>
      </div>
      
    </div>
    <span id="rytzee-servicess"></span>
  </div>
  <!-- About Section End -->
  
  <!-- About Section Start -->
  <div id="rytzee-services" >
    <div class="product-head slider-text-bottom">
      <p>
        Rytzee provides you a cloud based platform to easily setup and offer exclusive personalized experiences to your guests.
      </p>
      
    </div>
    <div class="container-fluid">
      <div class="row" id="product_2">
        <!-- <img src="<?php bloginfo( 'template_url' ); ?>/img/product/2.jpg" class="img-responsive"> -->
      </div>
      <div class="row" id="product_1">
        <!-- <img src="<?php bloginfo( 'template_url' ); ?>/img/product/2.jpg" class="img-responsive"> -->
      </div>
    </div>
  </div>
  <!-- About Section End -->
  <!-- About Section Start -->
  <div id="mobileapp" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-xs-4">
          <img src="<?php bloginfo( 'template_url' ); ?>/img/mobileapp/img-1.png" class="mobileapp-img">
        </div>
        <div class="col-lg-8 col-md-6 col-xs-6">
          <p class="mobileapp-p font-family-ptsans"><span class="mobileapp-span">Rytzee mobile app solutions will provide</span><br> the personal concierge experience to your guests.
        </div>
        
      </div>
    </div>
  </div>
  <!-- About Section End -->
  <!-- About Section Start -->
  <div id="mobileapplist">
    <div class="mobileapplist-backcolor">
      <div class="container">
        <div class="row">
          
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="service-box">
              <div class="service-icon">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/justfor.png" class="img-medium-icon">
              </div>
              <div class="service-contentt">
                <h4 class="service-h4">JUST FOR YOU</h4>
                <p class="service-p">View personalized offers, products, services and travel plans.</p>
              </div>
            </div>
            
          </div>
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="service-box">
              <div class="service-icon">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/pasword.png" class="img-medium-icon">
              </div>
              <div class="service-contentt">
                <h4 class="service-h4">DIGITAL PASS</h4>
                <p class="service-p">Digital pass of all events, activities and services in one place. </p>
              </div>
            </div>
            
          </div>
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="service-box">
              <div class="service-icon">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/explore.png" class="img-medium-icon">
              </div>
              <div class="service-contentt">
                <h4 class="service-h4">EXPLORE</h4>
                <p class="service-p">Explore other experiences, services and offers at the destination. </p>
                <p class="service-p">Mark interested ones as favorites or must go.  </p>
              </div>
            </div>
            
          </div>
        </div>
        <div class="line-hr-dash-fill">
          <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/arrow-cir.png" class="line-img-arrow">
        </div>
        <div class="row">
          
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="service-box">
              <div class="service-icon">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/map.png" class="img-medium-icon">
              </div>
              <div class="service-contentt">
                <h4 class="service-h4">MAP VIEW</h4>
                <p class="service-p">View your favorites and must go in a map view. </p>
                <p class="service-p">Easily plan your day or make well informed purchases with all relevant information in one place. </p>
                
              </div>
            </div>
            
          </div>
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="service-box">
              <div class="service-icon">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/cash.png" class="img-medium-icon">
              </div>
              <div class="service-contentt">
                <h4 class="service-h4">PURCHASE</h4>
                <p class="service-p">Pre-book services even before your travel. </p>
                <p class="service-p">Order services from anywhere, irrespective of whether you are on or off property. </p>
              </div>
            </div>
            
          </div>
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="service-box">
              <div class="service-icon">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/communicate-1.png" class="img-medium-icon">
              </div>
              <div class="service-contentt">
                <h4 class="service-h4">COMMUNICATE</h4>
                <p class="service-p">Guest can real time chat with the respective service provider. </p>
                <p class="service-p">Send messages to service providers</p>
                <p class="service-p">Provide feedback to the service provider</p>
              </div>
            </div>
            
          </div>
          
        </div>
        <div class="line-hr-dash-fill">
          <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/arrow-cir.png" class="line-img-arrow">
        </div>
        <div class="row">
          
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="service-box">
              <div class="service-icon">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/notification.png" class="img-medium-icon">
              </div>
              <div class="service-contentt">
                <h4 class="service-h4">NOTIFICATIONS</h4>
                <p class="service-p">Get notified on upcoming schedule along with travel time.  </p>
                <p class="service-p">Receive notifications on nearby offers and services. </p>
                
              </div>
            </div>
            
          </div>
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="service-box">
              <div class="service-icon">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/budget.png" class="img-medium-icon">
              </div>
              <div class="service-contentt">
                <h4 class="service-h4">BUDGET METER</h4>
                <p class="service-p">Define a budget for your trip. </p>
                <p class="service-p">Track the budget as you plan and experience your travel. </p>
              </div>
            </div>
            
          </div>
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="service-box">
              <div class="service-icon">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/docker.png" class="img-medium-icon">
              </div>
              <div class="service-contentt">
                <h4 class="service-h4">DOCKET</h4>
                <p class="service-p">Maintain your travel profile and preferences.  </p>
                
              </div>
            </div>
            
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- About Section End -->
  <!-- Contact Form Section Start -->
  <section id="contact" class="contact-form section-padding">
    <div class="container">
      
      
      <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12">
          <h5 class="text-left ">YOU WILL LEARN!</h5>
          <div class="line-hr"></div>
          <div class="row">
            <div class="col-md-10 col-lg-10 col-xs-10 div-border-bottom">
              <div class="service-box">
                <div class="service-icon">
                  <!-- <i class="fa fa-building"></i> -->
                  <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/checkmark.png" class="img-width-65">
                </div>
                <div class="service-content">
                  <p>
                    How you can integrate Rytzee with your existing systems and process
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-10 col-lg-10 col-xs-10 div-border-bottom">
              <div class="service-box">
                <div class="service-icon">
                  <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/checkmark.png" class="img-width-65">
                </div>
                <div class="service-content">
                  <p>
                    How Rytzee can help you generate incremental revenue
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-10 col-lg-10 col-xs-10 div-border-bottom">
              <div class="service-box">
                <div class="service-icon">
                  <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/checkmark.png" class="img-width-65">
                </div>
                <div class="service-content">
                  <p>
                    Our pricing model and illustrative Return on Investment
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-10 col-lg-10 col-xs-10 div-border-bottom">
              <div class="service-box">
                <div class="service-icon">
                  <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/checkmark.png" class="img-width-65">
                </div>
                <div class="service-content">
                  <p>
                    Customization and white labeling options.
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-10 col-lg-10 col-xs-10 div-border-bottom">
              <div class="service-box">
                <div class="service-icon">
                  <img src="<?php bloginfo( 'template_url' ); ?>/img/icons/checkmark.png" class="img-width-65">
                </div>
                <div class="service-content">
                  <p>
                    Or anything else you want to know.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-12">
          <h5 class="text-left">LET’S KNOW YOU BETTER</h5>
          <div class="line-hr"></div>
          <div class="contact-us">
            <form class="contact-form" data-toggle="validator">
              <div class="group">
                <input type="text" required data-error="Please enter your name" name="txtName" id="txtName" >
                <div class="help-block with-errors"></div>
                
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Name</label>
                
              </div>
              <div class="group">
                <input type="text"  name="txtBussinessName" id="txtBussinessName">
                <div class="help-block with-errors"></div>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Bussiness Name</label>
              </div>
              <div class="group">
                <input type="email" required data-error="Please enter your valid email" name="txtEmail" id="txtEmail">
                <div class="help-block with-errors"></div>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Email</label>
              </div>
              
              <div class="group">
                <input type="tel"  name="txtPhone" id="txtPhone">
                <div class="help-block with-errors"></div>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Phone</label>
              </div>
              <div class="group">
                <input type="text"  name="txtAddress" id="txtAddress">
                <div class="help-block with-errors"></div>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Address</label>
              </div>
              <div class="group">
                <input type="text"   name="txtWebsite" id="txtWebsite">
                <div class="help-block with-errors"></div>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Website</label>
              </div>
              <div class="group">
                <input type="number"   name="txtGuest" id="txtGuest">
                <div class="help-block with-errors"></div>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Average guest/Month</label>
              </div>
              <div class="group">
                <input type="text"  name="txtComment" id="txtCommen">
                <div class="help-block with-errors"></div>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Tell us more about yourself</label>
              </div>
              
              <button type="Submit" id="form-submit" class="btn btn-common btn-background-color">Submit</button>
              <div id="msgSubmit" class="h3 text-center hidden">
                
              </div>
            </form>
          </div>
          
        </div>
      </div>
    </div>
  </section>
  <!-- Contact Form Section End -->
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span>
          </button>
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always">></iframe>
          </div>
          
          
        </div>
      </div>
    </div>
  </div>
  <?php get_footer(); ?>