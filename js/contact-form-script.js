$("#form-submit").click(function () {
    if ($(".disabled")[0]) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Did you fill in the form properly?");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var name = $("#txtName").val();
    var bussinessname =$("#txtBussinessName").val();
    var email = $("#txtEmail").val();
    var phno = $("#txtPhone").val();
    var address = $("#txtAddress").val();
    var website = $("#txtWebsite").val();
    var guest = $("#txtGuest").val();
    var comment = $("#txtCommen").val();


    $.ajax({
        type: "POST",
        url: "wp-content/themes/Rytzee/php/form-process.php",
        data: "name=" + name + "&email=" + email + "&bussinessname=" + bussinessname + "&phno=" + phno+ "&address=" + address + "&website=" + website+ "&guest=" + guest+ "&comment=" + comment,
        success : function(text){
            if (text == "failed"){
                formError();
                submitMSG(false,text);
                
            } else {
               formSuccess();
            }
        }
    });
}

function formSuccess(){
     $("#txtName").val('');
    $("#txtBussinessName").val('');
    $("#txtEmail").val('');
    $("#txtPhone").val('');
    $("#txtAddress").val('');
    $("#txtWebsite").val('');
    $("#txtGuest").val('');
    $("#txtCommen").val('');
    submitMSG(true, "Message Submitted!")
}

function formError(){
    $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).html(msg);
    //$("#msgSubmit").html(msg);
}