
      <!-- Footer Section -->
      <footer class="footer">
        
        <!-- Copyright -->
        <div id="copyright">
          <div class="container">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-xs-4">
               
                <a href="https://www.linkedin.com/company/rytzee/" target="_blank"><i class="fa fa-linkedin"></i></a>
                |
                <a href="https://www.facebook.com/rytzee" target="_blank"><i class="fa fa-facebook"></i></a><br>
                <p class="copyright-text">copyright2018 www.rytzee.com
                </p>
              </div>
              <div class="col-lg-8 col-md-6 col-xs-8">
                <a href="#" class="navbar-brand pull-right footer-logo-img"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" alt=""></a>
              </div>
            </div>
            
          </div>
        </div>
        <!-- Copyright  End-->
      </footer>
      <!-- Footer Section End-->
      <!-- Go to Top Link -->
      <a href="#" class="back-to-top">
        <i class="fa fa-arrow-up"></i>
      </a>
      
      <!-- Preloader -->
      <div id="preloader">
        <div class="loader" id="loader-1"></div>
      </div>
      <!-- End Preloader -->
      
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery-min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/popper.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/bootstrap.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/owl.carousel.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.mixitup.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.countTo.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.nav.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/scrolling-nav.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.easing.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.slicknav.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/validator.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/contact-form-script.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/main.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/video.js"></script>
      
    </body>
  </html>