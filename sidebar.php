
    <!-- Header Area wrapper Starts -->
    <header id="header-wrap" id="home">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            <span class="icon-menu"></span>
            <span class="icon-menu"></span>
            <span class="icon-menu"></span>
            </button>
            <a href="#" class="navbar-brand"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" alt=""></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
              <li class="nav-item active">
                <a class="nav-link" href="#sliders">
                  HOME
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#benefit">
                  BENEFIT
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#rytzee-servicess">
                  PRODUCT
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact">
                  CONTACT  US
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#blogs">
                  BLOGS
                </a>
              </li>
              
              
            </ul>
          </div>
        </div>
        <!-- Mobile Menu Start -->
        <ul class="mobile-menu navbar-nav">
          <li>
            <a class="page-scroll" href="#sliders">
              HOME
            </a>
          </li>
          <li>
            <a class="page-scroll" href="#benefit">
              BENEFIT
            </a>
          </li>
          <li>
            <a class="page-scroll" href="#rytzee-servicess">
              PRODUCT
            </a>
          </li>
          <li>
            <a class="page-scroll" href="#contact">
              CONTACT US
            </a>
          </li>
          <li>
            <a class="page-scroll" href="#blogs">
              BLOGS
            </a>
          </li>
          
        </ul>
        <!-- Mobile Menu End -->
      </nav>
      <!-- Navbar End -->
      <!-- sliders -->
      <div id="sliders">
        <div class="full-width">
          <!-- light slider -->
          <div id="light-slider" class="carousel slide">
            <div id="carousel-area">
              <div id="carousel-slider" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                    <img src="<?php bloginfo( 'template_url' ); ?>/img/slider/bg-2.jpg" alt="">
                    <div class="carousel-caption">
                      <h3 class="slide-title animated fadeInDown"><span>Rytzee</span></h3>
                      <h5 class="font-family-ptsans font-size-40">Luxury Made Affordable</h5>
                      <h3 class="slide-text animated fadeIn font-family-ptsans font-size-20">It has never been easier to connect your guests to exclusive services,
                      enhance customer satisfaction and earn incremental revenue.</h3>
                      <a href="#benefit" class="btn btn-lg btn-common animated fadeInUp">Learn How ?</a>
                      
                    </div>
                  </div>
                  
                  
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End sliders -->
    </header>
    <!-- Header Area wrapper End -->